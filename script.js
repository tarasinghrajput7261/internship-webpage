function toggleNavbar() {
  var navbar = document.querySelector('.navbar');
  var navbarMenu = document.querySelector('.navbar-menu');
  var navbarToggle = document.querySelector('.navbar-toggle');
  navbar.classList.toggle('active');
  navbarMenu.classList.toggle('active');
  navbarToggle.classList.toggle('active');
}

const navbarItems = document.querySelectorAll('.navbar-menu');

navbarItems.forEach((item) => {
  item.addEventListener('click', () => {
    const navbarToggle = document.getElementById('navbar-toggle');
    const navbarMenu = document.getElementById('navbar-menu');
    const navbar = document.getElementById('navbar');

    if (window.innerWidth <= 600) {
      navbarToggle.classList.remove('active');
      navbarMenu.classList.remove('active');
      navbar.classList.remove('active');
    }
  });
});
